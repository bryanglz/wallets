<?php

function fiatNormalizeNear($balanceStr)
{
    return (float)substr($balanceStr, 0, -20) / 10000;
}

function getNearBalanceUSD($wallet)
{
    $url = "https://api.app.astrodao.com/api/v1/stats/dao/{$wallet}/state";
    $response = file_get_contents($url);
    $data = json_decode($response, true);
    return $data['totalDaoFunds']['value'];
}

function getNearWalletUSD($wallet)
{
    $result = [];
    $url = "https://api.app.astrodao.com/api/v1/tokens/account-tokens/{$wallet}";
    $response = file_get_contents($url);
    $data = json_decode($response, true);
    foreach ($data as $row) {
        $fiatConversion = (float)$row['price'];
        $balance = (float)substr($row['balance'], 0, -1 * $row['decimals'] + 2) / 100;
        $result[$row['symbol']] = [
            'fiatConversion' => $fiatConversion,
            'fiatBalance' => $balance * $fiatConversion,
            'usd' => $balance * $fiatConversion,
        ];
    }
    return $result;
}

function getNearOutgoingUSD($wallet, $fiatConversion)
{
    // https://api.app.astrodao.com/api/v1/transactions/receipts/account-receipts/unchain-fund.sputnik-dao.near

//https://api.app.astrodao.com/api/v1/transactions/receipts/account-receipts/unchain-fund.sputnik-dao.near/tokens/6b175474e89094c44da98b954eedeac495271d0f.factory.bridge.near

// https://api.app.astrodao.com/api/v1/transactions/receipts/account-receipts/unchain-fund.sputnik-dao.near/tokens/f5cfbc74057c610c8ef151a439252680ac68c6dc.factory.bridge.near

// https://api.app.astrodao.com/api/v1/transactions/receipts/account-receipts/unchain-fund.sputnik-dao.near/tokens/xtoken.ref-finance.near

    $urlNearHistory = "https://api.app.astrodao.com/api/v1/transactions/receipts/account-receipts/{$wallet}";
    $responseNearHistory = file_get_contents($urlNearHistory);
    $dataNearHistory = json_decode($responseNearHistory, true);
// receiptActions[actionKind='TRANSFER'][receiptReceiverAccountId!='unchain-fund.sputnik-dao.near'].args.deposit
    $result = 0;
    $i = 0;
    printf("Near:\n");
    foreach ($dataNearHistory as $transaction) {
        if ($transaction["predecessorAccountId"] == $wallet) {
            $actions = $transaction['receiptActions'];
            $dt = date('c', substr($transaction['includedInBlockTimestamp'], 0, -9));
            foreach ($actions as $action) {
                if ($action['actionKind'] === 'TRANSFER'
                    && $action['receiptReceiverAccountId'] !== $wallet) {
                    $deposit = $action['args']['deposit'];
                    $fiatBalance = fiatNormalizeNear($deposit);
                    printf("%03u) [%s] %s %s\n", ++$i, $dt, $deposit, $fiatBalance);
                    $result += $fiatBalance * $fiatConversion;
                }
            }
        }
    }
    return $result;
}

function getPolkadotBalanceUSD($wallet)
{
    $url = "https://api.blockchair.com/polkadot/raw/address/{$wallet}?limit=10&offset=0";
    $response = file_get_contents($url);
    $data = json_decode($response, true);
    $balance = 0.0;
    if (isset($data['data'][$wallet]['address']['balance']['free'])) {
        $balance = (float)substr($data['data'][$wallet]['address']['balance']['free'], 0, -8) / 100;
    }
    $fiatConversion = $data['context']['market_price_usd'];
    return $balance * $fiatConversion;
}

function renderHtml($totalUSD, $walletUSD)
{
    $list = '';
    foreach ($walletUSD as $key => $row) {
        $list .= "<li>{$key}: {$row['usd']}</li>";
    }

    $html = <<< HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Balance</title>
</head>
<body>
<div> TotalFiat: $totalUSD </div>
<ul>{$list}</ul>
</body>
</html>
HTML;

    file_put_contents('balance.html', $html);
}

function renderJson($totalUSD, $walletUSD)
{
    // filter empty wallets
    $walletUSD = array_filter($walletUSD, function ($row) {
        return $row['usd'] > 0;
    }, ARRAY_FILTER_USE_BOTH);
// save json
    $walletJsonFormat = array_map(static function ($key, $wallet) {
        return [
            'key' => $key,
            'usd' => round($wallet['usd'], 2)
        ];
    }, array_keys($walletUSD), array_values($walletUSD));

    $response = [
        'wallets' => [
            'totalUSD' => round($totalUSD, 2),
            'walletUSD' => $walletJsonFormat,
            'updatedAt' => date('r')
        ]
    ];
    file_put_contents("data.json", json_encode($response));
}